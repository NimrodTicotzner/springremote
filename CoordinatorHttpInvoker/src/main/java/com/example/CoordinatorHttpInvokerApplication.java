package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.httpinvoker.HttpInvokerServiceExporter;
import org.springframework.web.context.support.HttpRequestHandlerServlet;

import com.example.services.CoordinatorService;
import com.example.services.CoordinatorServiceImpl;

@SpringBootApplication
@ServletComponentScan
public class CoordinatorHttpInvokerApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(CoordinatorHttpInvokerApplication.class, args);
	}

	@Bean
	public CoordinatorService coordinatorService() {
		return new CoordinatorServiceImpl();
	}

	@Bean(name = "httpRequestHandlerServlet")
	public HttpInvokerServiceExporter serviceExporter() {
		HttpInvokerServiceExporter serviceExporter = new HttpInvokerServiceExporter();
		serviceExporter.setService(coordinatorService());
		serviceExporter.setServiceInterface(CoordinatorService.class);
		return serviceExporter;
	}

	@Bean
	public ServletRegistrationBean servletRegistrationBean() {
		return new ServletRegistrationBean(new HttpRequestHandlerServlet(), "/remoting/CoordinatorService");
	}

}
