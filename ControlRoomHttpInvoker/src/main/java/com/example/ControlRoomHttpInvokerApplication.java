package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;

import com.example.services.CoordinatorService;

@SpringBootApplication
public class ControlRoomHttpInvokerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControlRoomHttpInvokerApplication.class, args);
	}

	@Bean
	public HttpInvokerProxyFactoryBean coordinatorService() {
		HttpInvokerProxyFactoryBean factoryBean = new HttpInvokerProxyFactoryBean();
		factoryBean.setServiceUrl("http://127.0.0.1:8080/remoting/CoordinatorService");
		factoryBean.setServiceInterface(CoordinatorService.class);
		return factoryBean;
	}

}
