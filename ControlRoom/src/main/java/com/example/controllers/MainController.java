package com.example.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.services.CoordinatorService;

@RestController
@RequestMapping("/control")
public class MainController {

	@Autowired
	private CoordinatorService coordinatorService;

	@RequestMapping(value = "/names", method = RequestMethod.GET)
	public String getNamesFromCoordinator() {
		return coordinatorService.getNames();
	}

}
