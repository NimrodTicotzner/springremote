package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

import com.example.services.CoordinatorService;

@SpringBootApplication
public class ControlRoomApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControlRoomApplication.class, args);
	}

	@Bean
	public RmiProxyFactoryBean coordinatorService() {
		RmiProxyFactoryBean bean = new RmiProxyFactoryBean();
		bean.setServiceUrl("rmi://10.0.1.128:1199/CoordinatorService");
		bean.setServiceInterface(CoordinatorService.class);
		return bean;
	}
}
