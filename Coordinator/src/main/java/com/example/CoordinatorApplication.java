package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.rmi.RmiServiceExporter;

import com.example.services.CoordinatorService;
import com.example.services.CoordinatorServiceImpl;

@SpringBootApplication
public class CoordinatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoordinatorApplication.class, args);
	}

	@Bean
	public CoordinatorService coordinatorService() {
		return new CoordinatorServiceImpl();
	}

	@Bean
	public RmiServiceExporter serviceExporter() {
		RmiServiceExporter serviceExporter = new RmiServiceExporter();
		serviceExporter.setServiceName("coordinatorService");
		serviceExporter.setService(coordinatorService());
		serviceExporter.setServiceInterface(CoordinatorService.class);
		serviceExporter.setRegistryPort(1199);
		return serviceExporter;
	}
}
